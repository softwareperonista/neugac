/*
 * neugac-usuario.c
 *
 * Copyright (C) 2014 - Andres Fernandez
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "neugac-usuario.h"

Usuario
neugac_usuario_new ( guint id )
{
	Usuario usuario;
	usuario.id = id;
	return usuario;
}

void
neugac_usuario_set_nombre ( Usuario* usuario, gchar nombre )
{
	usuario->nombre = nombre;
}

void
neugac_usuario_print ( Usuario* usuario )
{
	printf ( "Usuario:\nId: %d\n", usuario->id);
}