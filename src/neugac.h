/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * neugac.h
 * Copyright (C) 2014 Andres Fernandez <andres@localhost>
 * 
 * Neugac is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Neugac is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NEUGAC_
#define _NEUGAC_

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define NEUGAC_TYPE_APPLICATION             (neugac_get_type ())
#define NEUGAC_APPLICATION(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), NEUGAC_TYPE_APPLICATION, Neugac))
#define NEUGAC_APPLICATION_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), NEUGAC_TYPE_APPLICATION, NeugacClass))
#define NEUGAC_IS_APPLICATION(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), NEUGAC_TYPE_APPLICATION))
#define NEUGAC_IS_APPLICATION_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), NEUGAC_TYPE_APPLICATION))
#define NEUGAC_APPLICATION_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), NEUGAC_TYPE_APPLICATION, NeugacClass))

typedef struct _NeugacClass NeugacClass;
typedef struct _Neugac Neugac;
typedef struct _NeugacPrivate NeugacPrivate;

struct _NeugacClass
{
	GtkApplicationClass parent_class;
};

struct _Neugac
{
	GtkApplication parent_instance;

	NeugacPrivate *priv;

};

GType neugac_get_type (void) G_GNUC_CONST;
Neugac *neugac_new (void);

/* Callbacks */

G_END_DECLS

#endif /* _APPLICATION_H_ */

