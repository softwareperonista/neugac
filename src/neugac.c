/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * neugac.c
 * Copyright (C) 2014 Andres Fernandez <andres@localhost>
 * 
 * Neugac is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Neugac is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "neugac.h"

#include <glib/gi18n.h>
#include "neugac-usuario.h"



/* For testing propose use the local (not installed) ui file */
/* #define UI_FILE PACKAGE_DATA_DIR"/ui/neugac.ui" */
#define UI_FILE "src/neugac.ui"
#define TOP_WINDOW "ventana_principal"


G_DEFINE_TYPE (Neugac, neugac, GTK_TYPE_APPLICATION);

/* ANJUTA: Macro NEUGAC_APPLICATION gets Neugac - DO NOT REMOVE */
struct _NeugacPrivate
{
	/* ANJUTA: Widgets declaration for neugac.ui - DO NOT REMOVE */
};

/* Create a new window loading a file */
static void
neugac_new_window (GApplication *app,
                           GFile        *file)
{
	GtkWidget *window;

	GtkBuilder *builder;
	GError* error = NULL;

	NeugacPrivate *priv = NEUGAC_APPLICATION(app)->priv;

	/* Load UI from file */
	builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (builder, UI_FILE, &error))
	{
		g_critical ("Couldn't load builder file: %s", error->message);
		g_error_free (error);
	}

	/* Auto-connect signal handlers */
	gtk_builder_connect_signals (builder, app);

	/* Get the window object from the ui file */
	window = GTK_WIDGET (gtk_builder_get_object (builder, TOP_WINDOW));
        if (!window)
        {
		g_critical ("Widget \"%s\" is missing in file %s.",
				TOP_WINDOW,
				UI_FILE);
        }

	
	/* ANJUTA: Widgets initialization for neugac.ui - DO NOT REMOVE */

	g_object_unref (builder);
	
	
	gtk_window_set_application (GTK_WINDOW (window), GTK_APPLICATION (app));
	if (file != NULL)
	{
		/* TODO: Add code here to open the file in the new window */
	}

	gtk_widget_show_all (GTK_WIDGET (window));
}

/* Implementación de callbacks */
void
neugac_a_lista_intimar (GtkTreeView* usuarios,
                        GtkTreePath* path,
                        GtkTreeViewColumn* column,
                        gpointer* user_data)
{
	Usuario usuario;
	GtkTreeIter iter;
	guint* id;
	gchar* nombre;
	GtkTreeModel* model;
	GtkTreeSelection* seleccion = gtk_tree_view_get_selection (GTK_TREE_VIEW(usuarios));

	gtk_tree_selection_get_selected ( GTK_TREE_SELECTION(seleccion), &model, &iter );
	gtk_tree_model_get (model, &iter, 1, &nombre, -1);
	gtk_tree_model_get (model, &iter, 0, &id, -1);
	gtk_list_store_append  ( GTK_LIST_STORE (user_data), &iter);
	gtk_list_store_set (GTK_LIST_STORE(user_data), &iter, 0, nombre, -1);	

	usuario = neugac_usuario_new (&id);
	neugac_usuario_set_nombre (&usuario, &nombre);
	neugac_usuario_print (&usuario);
}

/* GApplication implementation */
static void
neugac_activate (GApplication *application)
{
	neugac_new_window (application, NULL);
}

static void
neugac_open (GApplication  *application,
                     GFile        **files,
                     gint           n_files,
                     const gchar   *hint)
{
	gint i;

	for (i = 0; i < n_files; i++)
		neugac_new_window (application, files[i]);
}

static void
neugac_init (Neugac *object)
{
	object->priv = G_TYPE_INSTANCE_GET_PRIVATE (object, NEUGAC_TYPE_APPLICATION, NeugacPrivate);
}

static void
neugac_finalize (GObject *object)
{
	G_OBJECT_CLASS (neugac_parent_class)->finalize (object);
}

static void
neugac_class_init (NeugacClass *klass)
{
	G_APPLICATION_CLASS (klass)->activate = neugac_activate;
	G_APPLICATION_CLASS (klass)->open = neugac_open;

	g_type_class_add_private (klass, sizeof (NeugacPrivate));

	G_OBJECT_CLASS (klass)->finalize = neugac_finalize;
}

Neugac *
neugac_new (void)
{
	return g_object_new (neugac_get_type (),
	                     "application-id", "org.gnome.neugac",
	                     "flags", G_APPLICATION_HANDLES_OPEN,
	                     NULL);
}

