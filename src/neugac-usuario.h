/*
 * neugac-usuario.h
 *
 * Copyright (C) 2014 - Andres Fernandez
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NEUGAC_USUARIO_
#define _NEUGAC_USUARIO_

#include <glib/gi18n.h>

typedef struct
{
	guint id;
	gchar nombre;
} Usuario;

Usuario neugac_usuario_new ( guint id);
void neugac_usuario_set_nombre ( Usuario* usuario, gchar nombre );
void neugac_usuario_print ( Usuario* usuario );

#endif